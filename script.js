//Author: Dema Siva Mangala Gowridevi Akasapu
//Student ID: 8764398


//declaration of form variables
var formName, formPostcode, numColourPrints, numGrayscalePrints, numScans;

//Using Jquery for toggle button from Home Page
$(document).ready(function() {

    $('#togglebuttonCP').click(function() {
        $('#hideThisCP').fadeToggle(1000);
    })

    $('#togglebuttonGP').click(function() {
        $('#hideThisGP').fadeToggle(1000);
    })

    $('#togglebuttonS').click(function() {
        $('#hideThisS').fadeToggle(1000);
    })

    $('#togglebuttonAbout').click(function() {
        $('#aboutCompany').fadeToggle(1000);
    })
})

//function invokes on form submit
function formSubmit() {

    //initialization of error variables
    var errCount = 0;
    var errors = "";

    //fetching element values from html
    formName = document.getElementById('customerName').value;
    formPostcode = document.getElementById('postalcode').value;
    numColourPrints = document.getElementById('colourPrints').value;
    numGrayscalePrints = document.getElementById('grayscalePrints').value;
    numScans = document.getElementById('scans').value;

    //customer name validation
    var numbersOnlyRegex = /^[0-9]+$/;
    if (formName == '' || formName == null || numbersOnlyRegex.test(formName)) {
        errors += 'Please enter a valid customer name <br>';
        errCount++;
    }

    //postal code validation
    var postcodeRegex = /^[A-Za-z][0-9][A-Za-z]\s[0-9][A-Za-z][0-9]$/;
    if (!postcodeRegex.test(formPostcode)) {
        errors += 'Postcode should be in the format N2B 5G3 <br>';
        errCount++;
    }

    //validating the customer detials and any one of the item valid quantity 
    if ((!numColourPrints || !numbersOnlyRegex.test(numColourPrints)) &&
        (!numGrayscalePrints || !numbersOnlyRegex.test(numGrayscalePrints)) &&
        (!numScans || !numbersOnlyRegex.test(numScans))) {
        errCount++;
        errors += 'Please select valid item quantity<br> ';
    }

    //checking for the errors from the above conditions
    if (errors != 0) {
        document.getElementById('errors').innerHTML = errors;
        return false;
    }

    //validating the no.of colour prints
    if (!!numColourPrints) {
        if (!numbersOnlyRegex.test(numColourPrints.trim())) {
            errors += 'Please enter valid no.of colour prints <br>';
            errCount++;
        }
    }

    //validating the no.of gray prints
    if (!!numGrayscalePrints) {
        if (!numbersOnlyRegex.test(numGrayscalePrints.trim())) {
            errors += 'Please enter valid no.of grayscale prints <br>';
            errCount++;
        }
    }

    //validating the no.of scans
    if (!!numScans) {
        if (!numbersOnlyRegex.test(numScans.trim())) {
            errors += 'Please enter valid no.of scans <br>';
            errCount++;
        }
    }

    //condition to check the errors, if not display the generated invoice
    if (errors != '') {
        //display the errors present
        document.getElementById('errors').innerHTML = errors;
        document.getElementsByClassName('formData')[0].style.display = "none";
        document.getElementById("formResult").style.display = "none";

    } else {
        //clear errors
        document.getElementById('errors').innerHTML = '';
        document.getElementsByClassName('formData')[0].style.display = "block";
        document.getElementById("formResult").style.display = "block";
        //function call to generate invoice
        generateInvoice();

    }
    // Return false will stop the form from submitting and keep it on the current page.
    return false;
}

//function to generate the Invoice
function generateInvoice() {

    //constant item costs
    const COLOURPRINTSCOST = 0.50;
    const GRAYSCALEPRINTSCOST = 0.20;
    const SCANSCOST = 0.25;

    // calculation of item prices
    var colorPrintsPrice = !!numColourPrints ? numColourPrints * parseFloat(COLOURPRINTSCOST) : 0;
    var grayscalePrintsPrice = !!numGrayscalePrints ? numGrayscalePrints * parseFloat(GRAYSCALEPRINTSCOST) : 0;
    var scansPrice = !!numScans ? numScans * parseFloat(SCANSCOST) : 0;
    var itemsPrice = colorPrintsPrice + grayscalePrintsPrice + scansPrice;

    // calculation of tax
    var tax = 0.13 * itemsPrice;

    // calculation of total cost including tax
    var finalCost = itemsPrice + tax;

    //rounding off
    colorPrintsPrice = colorPrintsPrice.toFixed(2);
    grayscalePrintsPrice = grayscalePrintsPrice.toFixed(2);
    scansPrice = scansPrice.toFixed(2);
    tax = tax.toFixed(2);
    itemsPrice = itemsPrice.toFixed(2);
    finalCost = finalCost.toFixed(2);

    //table to generate invoice
    var invoice = '<table style="width: 100%; border-collapse: collapse;" border=1 >';
    invoice += '<tr><th colspan=2>Customer Name:</th><td colspan=2>' + formName + '</td></tr>';
    invoice += '<tr><th colspan=2>Postal Code:</th><td colspan=2>' + formPostcode + '</td></tr>';
    invoice += '<tr><th>Item Name</th><th>Item Quantity</th><th>Unit Price</th><th>Total Item Price</th></tr>';

    if (!!numColourPrints) {
        invoice += '<tr><td>Colour Prints</td><td style="text-align: center;">' + numColourPrints + '</td><td>' + `$${COLOURPRINTSCOST}` + '</td><td>' + `$${colorPrintsPrice}` + '</td></tr>';
    }
    if (!!numGrayscalePrints) {
        invoice += '<tr><td>Grayscale Prints</td><td style="text-align: center;">' + numGrayscalePrints + '</td><td>' + `$${GRAYSCALEPRINTSCOST}` + '</td><td>' + `$${grayscalePrintsPrice}` + '</td></tr>';
    }
    if (!!numScans) {
        invoice += '<tr><td>Scans</td><td style="text-align: center;">' + numScans + '</td><td>' + `$${SCANSCOST}` + '</td><td>' + `$${scansPrice}` + '</td></tr>';
    }
    invoice += '<tr><th colspan=3 >Sub Total</th><td>' + `$${itemsPrice}` + '</td></tr>';
    invoice += '<tr><th colspan=3 >Tax - 13%</th><td>' + `$${tax}` + '</td></tr>';
    invoice += '<tr><th colspan=3>Total Cost</th><td>' + `$${finalCost}` + '</td></tr>';
    invoice += '</table>';

    //display the formResult
    document.getElementById("formResult").style.display = "block";
    document.getElementById("formResult").innerHTML = invoice;
}
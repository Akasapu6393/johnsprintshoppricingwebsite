

## README
Please follow the steps in the README.md file before proceed to use the project

## ABOUT

This John's Printshop Pricing website gives the users to shop the different available paper printing options from the wesite.
You can customize this website to suite your specific requirements and easily integrate to the other systems.

## LICENSE

MIT License

Copyright (c) 2021 gowrideviakasapu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## REASON FOR LICENSE
MIT license is easily available class of licenses, users do not have to provide any other source code when releasing new software. 
The original MIT license in the reused code will make it suitable for commercial use.

## HOW TO INSTALL
1. Install the Visual Studio Code App
2. Download this project repository and open Visual Studio Code with this repository
3. Now you have access to the HTML Pages, Media, CSS Pages etc
4. Open the project repository in your computer 
5. Double Click on HomePage.html and project will open on your default browser
